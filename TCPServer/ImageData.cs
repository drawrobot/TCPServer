﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TCPServer
{
    internal class ImageData
    {
        public ImageData()
        {
            UploadDate = DateTime.Now;
        }

        [Required, Column(TypeName = "image")]
        public byte[] Image { get; set; }

        [Column(TypeName = "image")]
        public byte[] SmallImage { get; set; }

        public int Id { get; set; }

        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public DateTime UploadDate { get; set; }
    }
}