﻿using System.Data.Entity;

namespace TCPServer
{
    internal class DatabaseContext : DbContext
    {
        public DatabaseContext()
            : base(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Sir DrawALot;Integrated Security=True")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<ImageData> Images { get; set; }
    }
}