﻿namespace TCPServer
{
    internal enum Priority
    {
        Fatal,
        Error,
        Warn,
        Normal,
        Info,
        Success,
        Debug,
        Trace
    }
}