﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RestSharp;

namespace TCPServer
{
    internal class Program
    {
        #region P/Invoke

        // P/Invoke:
        private enum StdHandle { Stdin = -10, Stdout = -11, Stderr = -12 };
        [DllImport("kernel32.dll")]
        private static extern IntPtr GetStdHandle(StdHandle std);
        [DllImport("kernel32.dll")]
        private static extern bool CloseHandle(IntPtr hdl);

        #endregion

        #region localFields

        private const int Port = 1521;
        static int WIDTH = 1160; //Distance between the two lines on the frame
        private static PerformanceCounter _cpuCounter;
        private static DateTime _start;
        private static DatabaseContext _databaseContext;
        private static StreamWriter _raspberryPi;
        private static ImageProcessing.Pen _pen;

        public static StreamWriter RaspberryPi
        {
            get
            {
                return _raspberryPi;
            }
            set
            {
                _raspberryPi = value;
            }
        }
        #endregion

        private static void Main()
        {
             _start = DateTime.Now;
            StyleConsole();
            DisplayLine("server is starting ...", Priority.Info);
            InitCpu();
            var tcpServerRunThread = new Thread(TcpServerRun);
            _databaseContext = new DatabaseContext();
            InitDatabase();
            //Default values
            _pen = new ImageProcessing.Pen() { L = Math.Sqrt(200 * 200 + 200 * 200), R = Math.Sqrt(200 * 200 + (WIDTH - 200) * (WIDTH - 200)) };

            tcpServerRunThread.Start();
            DisplayLine("server started!", Priority.Success);

            string command = "";
            while (command != "quit")
            {
                command = Console.ReadLine();
                switch (command)
                {
                    case "help":
                    case "h":
                        DisplayHelp();
                        break;
                    case "q":
                    case "quit":
                    case "exit":
                        command = "quit";
                        break;
                    case "clear":
                    case "cls":
                    case "c":
                        Console.Clear();
                        break;
                    case "config":
                        Console.Write("Bitte x Koordinate eingeben [mm]: ");
                        int x = int.Parse("0" + Console.ReadLine());
                        Console.Write("Bitte y Koordinate eingeben [mm]: ");
                        int y = int.Parse("0" + Console.ReadLine());
                        _pen = new ImageProcessing.Pen() { L = Math.Sqrt(x * x + y * y), R = Math.Sqrt(y * y + (WIDTH - x) * (WIDTH - x)) };
                        break;
                    case "s":
                    case "status":
                        DisplayStatus();
                        break;
                    case "g":
                    case "get":
                        GetImageInfo();
                        break;
                    default:
                        if (command != "")
                        {
                            DisplayLine("command not found", Priority.Warn);
                            DisplayHelp();
                        }
                        break;
                }
            }
            Environment.Exit(0);
        }

        /// <summary>
        ///     This thread listens for incoming tcp connections and starts a new thread for each of them
        /// </summary>
        private static void TcpServerRun()
        {
            var tcpListener = new TcpListener(IPAddress.Any, Port);
            tcpListener.Start();
            while (true)
            {
                TcpClient client = tcpListener.AcceptTcpClient();
                var tcpHandlerThread = new Thread(tcpHandler);
                tcpHandlerThread.Start(client);
            }
        }

        /// <summary>
        ///     Starting a new thread for every connected user
        /// </summary>
        /// <param name="socket"></param>
        private static void tcpHandler(object socket)
        {
            try
            {
                User currUser = null;
                var client = (TcpClient) socket;
                string ip = ((IPEndPoint) client.Client.RemoteEndPoint).Address.ToString();
                DisplayLine("Neuer Client connected! IP: " + ip, Priority.Info);
                NetworkStream stream = client.GetStream();
                string message = "";

                var streamReader = new StreamReader(client.GetStream(), Encoding.ASCII);
                var streamWriter = new StreamWriter(client.GetStream(), Encoding.ASCII);
                var bytes = new byte[1];
                bool firstRun = true;

                //authentication
                while (currUser == null)
                {
                    if (!firstRun)
                    {
                        streamWriter.WriteLine("00false");
                        streamWriter.Flush();
                    }
                    firstRun = false;
                    string tmp = streamReader.ReadLine();
                    if (tmp == "74feceabeb2da94eca5eba0cd11a713723adab589ba9778898768527c930967e")
                    {
                        InitRaspberryPi(socket as TcpClient);
                        return;
                    }
                    var name = tmp.Substring(2);
                    string username = name.Split(':')[0];
                    string password = GenerateMd5Hash(name.Split(':')[1]);
                    if(_databaseContext.Users.Any(user => user.Name == username && user.Password == password))
                        currUser = _databaseContext.Users.First(user => user.Name == username && user.Password == password);
                }
                //user proved his identity
                streamWriter.WriteLine("00true");
                streamWriter.Flush();
                DisplayLine("Neuer Client ( " + currUser.Name + " ) hat sich angemeldet!", Priority.Info);
                string deviceKey = streamReader.ReadLine();
                while (message != "99")
                {
                    message = streamReader.ReadLine();
                    if (message.Length < 2)
                    {
                        message = "null";
                    }
                    Debug.WriteLine(message);
                    int id = 0;
                    List<short> steps;
                    switch (message.Substring(0, 2))
                    {
                        case "00":
                            streamWriter.WriteLine("00true");
                            streamWriter.Flush();
                            break;
                        case "01": //new picture
                            try
                            {
                                bytes = Convert.FromBase64String(message.Substring(2));
                                Image imageData = ByteArrayToImage(bytes);
                                double x, y;
                                if (imageData.Width > imageData.Height)
                                {
                                    x = 300;
                                    y = 300.0*imageData.Height/imageData.Width;
                                }
                                else
                                {
                                    y = 300;
                                    x = 300 * imageData.Width / imageData.Height;
                                }
                                var tmp = new ImageData
                                {
                                    UserId = currUser.UserId,
                                    Image = ImageToByteArray(imageData),
                                    SmallImage = ImageToByteArray(imageData.GetThumbnailImage((int)x, (int)y, null, IntPtr.Zero))
                                };
                                _databaseContext.Images.Add(tmp);
                                _databaseContext.SaveChanges();
                                DisplayLine(currUser.Name + " hat Bild( " + tmp.Id + " ) gesendet!", Priority.Info);
                                streamWriter.WriteLine("01" + tmp.Id);
                                streamWriter.Flush();
                            }
                            catch (FormatException e)
                            {
                                DisplayLine("client sandte korruptes Bild!", Priority.Warn, true);
                            }
                            break;
                        case "02": //reply picture
                            id = Convert.ToInt32(message.Substring(2));
                            DisplayLine(currUser.Name + " hat Bild mit id= " + id + " angefragt", Priority.Info);
                            var image = GetImage(id, currUser);
                            string reply;
                            if (image == null)
                            {
                                reply = "false";
                            }
                            else
                            {
                                reply = Convert.ToBase64String(ImageToByteArray(image));
                            }
                            streamWriter.WriteLine("02" + reply);
                            streamWriter.Flush();
                            break;
                        case "03": //get pictures id
                            DisplayLine(currUser.Name + " hat Liste seiner Bilder angefragt", Priority.Info);
                            if (_databaseContext.Images.Any(data => data.UserId == currUser.UserId))
                            {
                                var tmp1 =
                                _databaseContext.Images.Where(data => data.UserId == currUser.UserId)
                                    .Select(data => new { data.Id });
                                string tmp = "";
                                foreach (var item in tmp1)
                                {
                                    tmp = tmp + item.Id + ";";
                                }
                                streamWriter.WriteLine("03" + tmp);
                            }
                            else
                            {
                                streamWriter.WriteLine("03");
                            }
                            streamWriter.Flush();
                            break;
                        case "04": //get picture
                            id = Convert.ToInt32(message.Substring(2).Split(':')[0]);
                            DisplayLine(currUser.Name + " hat Bild mit id= " + id + " angefragt", Priority.Info);
                            Bitmap imageTmp = (Bitmap)GetImage(id, currUser);
                            if (imageTmp == null)
                            {
                                reply = "false";
                            }
                            else
                            {
                                CalcImage(ref imageTmp, Convert.ToInt32(message.Substring(2).Split(':')[1]));
                                reply = Convert.ToBase64String(ImageToByteArray(imageTmp));
                            }
                            
                            streamWriter.WriteLine("02" + reply);
                            streamWriter.Flush();
                            break;
                        case "05"://Get small picture
                            id = Convert.ToInt32(message.Substring(2).Split(':')[0]);
                            
                            DisplayLine(currUser.Name + " hat Bild mit id= " + id + " angefragt", Priority.Info);
                            imageTmp = (Bitmap)GetImage(id, currUser, true);
                            if (imageTmp == null)
                            {
                                reply = "false";
                            }
                            else
                            {
                                reply = Convert.ToBase64String(ImageToByteArray(imageTmp));
                            }
                            
                            streamWriter.WriteLine("02" + reply);
                            streamWriter.Flush();
                            break;
                        case "06"://deleting picture
                            id = Convert.ToInt32(message.Substring(2));
                            DisplayLine(currUser.Name + " hat Bild mit id= " + id + " gelöscht", Priority.Info);
                            if (_databaseContext.Images.Any(data => data.Id == id && data.UserId == currUser.UserId))
                            {
                                _databaseContext.Images.Remove(
                                _databaseContext.Images.First(data => data.Id == id && data.UserId == currUser.UserId));
                                _databaseContext.SaveChanges();
                            }
                            else
                                DisplayLine("Bild mit id= " + id + " gibt es nicht!", Priority.Warn);
                            break;
                        case "07": //draw text
                            string text = message.Substring(2);
                            DisplayLine(currUser.Name + " hat einen Text gesendet: " + text, Priority.Success);
                            ImageProcessing.Alphabet alphabet = new ImageProcessing.Alphabet();
                            try
                            {
                                steps = alphabet.DrawText(text, _pen);
                                File.WriteAllText("output.txt", $"{WIDTH}\r\n{_pen.L}\r\n{_pen.R}\r\n" + steps.Aggregate("", (current, step) => current + step + "\r\n"));
                                if (RaspberryPi == null)
                                {
                                    DisplayLine("RaspberryPi not connected!", Priority.Warn);
                                }
                                RaspberryPi?.WriteLine(steps.Aggregate("", (current, step) => current + step));
                                RaspberryPi?.Flush();
                                SendNotification(deviceKey, "drawing text started!");
                            }
                            catch (Exception e)
                            {
                                DisplayLine("RaspberryPi not connected!", Priority.Error);
                                SendNotification(deviceKey, "RaspberryPi not connected or busy!");
                            }
                            break;
                        case "08"://start drawing picture
                            message = message.Substring(2);
                            Console.WriteLine(message);
                            id = int.Parse(message.Split(';')[0]);
                            int trackbar = int.Parse(message.Split(';')[1]);
                            DisplayLine($"{currUser.Name} wants to print the picture {id} with {trackbar}", Priority.Success);
                            Image tmpImage = GetImage(id, currUser, false);
                            if (tmpImage == null)
                            {
                                Console.WriteLine("Error with this Id wasn't found!");
                                continue;
                            }
                            Bitmap currentImage = new Bitmap(tmpImage);
                            //CalcImage(ref currentImage, trackbar);
                            ImageProcessing.Image imageGenerator = new ImageProcessing.Image(currentImage, 200)
                            {
                                SEPARATOR = trackbar
                            };
                            try
                            {
                                steps = imageGenerator.DrawImage(_pen);
                                File.WriteAllText("output.txt", $"{WIDTH}\r\n{_pen.L}\r\n{_pen.R}\r\n" + steps.Aggregate("", (current, step) => current + step + "\r\n"));
                                RaspberryPi.WriteLine(steps.Aggregate("", (current, step) => current + step));
                                RaspberryPi.Flush();
                            }
                            catch (Exception)
                            {
                                DisplayLine("RaspberryPi not connected!", Priority.Error);
                            }
                            SendNotification(deviceKey, "drawing image started!");
                            break;
                    }
                }
                DisplayLine("client " + currUser.Name + " disconnected!", Priority.Info, true);

                stream.Close();
                client.Close();
            }
            catch (IOException e)
            {
                DisplayLine("client disconnected! ", Priority.Warn, true);
            }
            catch (Exception e)
            {
                DisplayLine("client disconnected! ", Priority.Error, true);
            }
        }

        /// <summary>
        /// If a new Pi gets connected add it as primary printer
        /// </summary>
        /// <param name="socket"></param>
        private static void InitRaspberryPi(TcpClient socket)
        {
            RaspberryPi = new StreamWriter(socket.GetStream(), Encoding.ASCII);
            DisplayLine("RaspberryPI sich angemeldet!", Priority.Success);
            DisplayLine("Don't forget to set X / Y (config)", Priority.Info);
        }

        #region helperMethods

        public static void SendNotification(string deviceKey, string message)
        {
            DisplayLine("Sending notification... " + deviceKey, ConsoleColor.Blue);
            var client = new RestClient("http://gcm-http.googleapis.com/gcm/send");
            var request = new RestRequest(Method.POST);
            request.AddHeader("authorization", "key=AIzaSyAHJRIkVf9ZH1O6D-vNtvSIwM5PW4g02IY");
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "\t{\r\n\t\t\"collapse_key\" : \"score_update\",\r\n\t\t\"data\": {\r\n\t\t\t\"text\": \"" + message + "\"\r\n\t\t},\r\n\t\t\"registration_ids\": [\"" + deviceKey + "\"]\r\n\t}\r\n", ParameterType.RequestBody);
            var response = client.Execute(request);
            Console.WriteLine(response.Content);
        }

        /// <summary>
        /// Implements the Threshold for the given image and trackbarvalue
        /// </summary>
        /// <param name="imageTmp"></param>
        /// <param name="trackbar"></param>
        private static void CalcImage(ref Bitmap imageTmp, int trackbar)
        {
            unsafe
            {
                BitmapData bitmapData = imageTmp.LockBits(new Rectangle(0, 0, imageTmp.Width, imageTmp.Height), ImageLockMode.ReadWrite, imageTmp.PixelFormat);
                int bytesPerPixel = Bitmap.GetPixelFormatSize(imageTmp.PixelFormat) / 8;
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;
                byte* ptrFirstPixel = (byte*)bitmapData.Scan0;
                Parallel.For(0, heightInPixels, y =>
                {
                    byte* currentLine = ptrFirstPixel + (y * bitmapData.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        int oldBlue = currentLine[x];
                        int oldGreen = currentLine[x + 1];
                        int oldRed = currentLine[x + 2];
                        double tmp = (oldRed * 2 + oldBlue + oldGreen * 3) / 6.0 / 255.0 * 100.0;
                        if (tmp > trackbar)
                        {
                            currentLine[x] = 255;
                            currentLine[x + 1] = 255;
                            currentLine[x + 2] = 255;
                        }
                        else
                        {
                            currentLine[x] = 0;
                            currentLine[x + 1] = 0;
                            currentLine[x + 2] = 0;
                        }

                    }
                });
                imageTmp.UnlockBits(bitmapData);
            }
        }

        /// <summary>
        /// Requests an Image for a user and also checks if the user is allowed to see the image
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currUser"></param>
        /// <param name="small"></param>
        /// <returns></returns>
        private static Image GetImage(int id, User currUser, bool small = false)
        {
            if (_databaseContext.Images.Count(data => data.Id == id && data.UserId == currUser.UserId) > 0)
            {
                    
                if (!small)
                {
                    var request =
                    _databaseContext.Images.Where(data => data.Id == id)
                        .Select(data => new { data.Image })
                        .First();
                    return ByteArrayToImage(request.Image);
                }
                else
                {
                    var request =
                    _databaseContext.Images.Where(data => data.Id == id)
                        .Select(data => new { data.SmallImage })
                        .First();
                    return ByteArrayToImage(request.SmallImage);    
                }
                    
            }
            return null;
        }

        /// <summary>
        ///     Generates the Md5 Hash from the String text
        /// </summary>
        /// <param name="text"></param>
        /// <returns>Md5 Hash</returns>
        private static string GenerateMd5Hash(string text)
        {
            byte[] encodedPassword = new UTF8Encoding().GetBytes(text);

            // need MD5 to calculate the hash
            byte[] hash = ((HashAlgorithm) CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedPassword);

            // string representation (similar to UNIX format)
            string encoded = BitConverter.ToString(hash)
                // without dashes
                .Replace("-", string.Empty)
                // make lowercase
                .ToLower();
            return encoded;
        }

        /// <summary>
        ///     Initials the PerformanceCounter for displaying the status of the cpu
        /// </summary>
        private static void InitCpu()
        {
            _cpuCounter = new PerformanceCounter
            {
                CategoryName = "Processor",
                CounterName = "% Processor Time",
                InstanceName = "_Total"
            };
            _cpuCounter.NextValue();
        }

        /// <summary>
        ///     Initial the Database and inserts dummy data
        /// </summary>
        private static void InitDatabase()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DatabaseContext>());
            if (!_databaseContext.Users.Any())
            {
                User windi = new User() { Name = "Windi", Password = GenerateMd5Hash("password") };
                User rene = new User() { Name = "Rene", Password = GenerateMd5Hash("password") };
                List<User> user = new List<User> {windi, rene};
                _databaseContext.Users.AddRange(user);
                _databaseContext.SaveChanges();

                //Picture tmp = new Bitmap(@"C:\Users\Michael\Documents\Visual Studio 2013\Projects\TCPServer\TCPServer\image\windi.jpg");
                //_databaseContext.Images.Add(new ImageData() { Picture = ImageToByteArray(tmp), User = windi, SmallImage = ImageToByteArray(tmp.GetThumbnailImage(300, 300, null, IntPtr.Zero)) });
                //tmp = new Bitmap(@"C:\Users\Michael\Documents\Visual Studio 2013\Projects\TCPServer\TCPServer\image\rene.png");
                //_databaseContext.Images.Add(new ImageData() { Picture = ImageToByteArray(tmp), User = rene, SmallImage = ImageToByteArray(tmp.GetThumbnailImage(300, 300, null, IntPtr.Zero)) });
                //_databaseContext.SaveChanges();
            }           
        }

        /// <summary>
        ///     Sets the title, width and height of the console
        /// </summary>
        private static void StyleConsole()
        {
            Console.Title = "Sir DrawALot - Server";
            Console.ForegroundColor = ConsoleColor.White;
            Console.WindowWidth = 100;
            Console.WindowHeight = 50;
        }

        #endregion

        #region commands

        /// <summary>
        ///     Displays a short overview of the current status from the server with infos like ip, port, memory usage and so on
        /// </summary>
        private static void DisplayStatus()
        {
            Hr();
            Console.WriteLine("Statusübersicht: ");
            string sHostName = Dns.GetHostName();
            IPHostEntry ipE = Dns.GetHostByName(sHostName);
            IPAddress[] ipA = ipE.AddressList;

            Console.Write("Aktuelle IP: ");
            DisplayLine(ipA[0].ToString(), ConsoleColor.DarkCyan, true, false);
            Console.Write("Aktueller Port: ");
            DisplayLine(Port.ToString(), ConsoleColor.DarkCyan, true, false);
            // get the physical mem usage
            double totalBytesOfMemoryUsed = Math.Round(Convert.ToDouble(Environment.WorkingSet) / 1024 / 1024, 2);
            Console.Write("Aktueller RAM Verbrauch: ");
            DisplayLine((totalBytesOfMemoryUsed).ToString(), ConsoleColor.DarkCyan, false, false);
            Console.WriteLine(" MB");
            Console.Write("Aktuelle CPU Auslastung: ");
            DisplayLine(Math.Round(_cpuCounter.NextValue(), 2).ToString(), ConsoleColor.DarkCyan, false, false);
            Console.WriteLine(" %");
            Console.WriteLine("Uptime: {0} Tage, {1} Stunden, {2} Minuten, {3} Sekunden", (DateTime.Now - _start).Days,
                (DateTime.Now - _start).Hours, (DateTime.Now - _start).Minutes, (DateTime.Now - _start).Seconds);
            Hr();
        }

        /// <summary>
        ///     Displays a short overview of all command from the server
        /// </summary>
        private static void DisplayHelp()
        {
            Hr();
            Console.WriteLine("Befehlsübersicht: ");
            DisplayLine("h, help".PadRight(45), ConsoleColor.DarkCyan, false);
            Console.WriteLine("Übersicht der Befehle anzeigen");
            DisplayLine("config".PadRight(45), ConsoleColor.DarkCyan, false);
            Console.WriteLine("Pen konfigurieren");
            DisplayLine("s, status".PadRight(45), ConsoleColor.DarkCyan, false);
            Console.WriteLine("Status des Servers");
            DisplayLine("c, cls, clear".PadRight(45), ConsoleColor.DarkCyan, false);
            Console.WriteLine("Inhalt des Terminals löschen");
            DisplayLine("q, quit, exit".PadRight(45), ConsoleColor.DarkCyan, false);
            Console.WriteLine("Server beenden");
            DisplayLine("g, get".PadRight(45), ConsoleColor.DarkCyan, false);
            Console.WriteLine("Bildinformationen abfragen");
            Hr();
        }

        /// <summary>
        ///     Displays a information overview of the image with the id = id
        /// </summary>
        private static void GetImageInfo()
        {
            Hr();
            Console.Write("Id des Bildes: ");
            string tmp = Console.ReadLine();
            int id;
            if (int.TryParse(tmp, out id))
            {
                if (_databaseContext.Images.Count(data => data.Id == id) > 0)
                {
                    var request = _databaseContext.Images.Where(data => data.Id == id)
                        .Select(data => new { data.Image })
                        .First();
                    Image image = ByteArrayToImage(request.Image);
                    Console.WriteLine("Id des Bildes: " + id);
                    Console.WriteLine("Größe des Bildes: " + request.Image.Length + " bytes");
                    Console.WriteLine("Width: {0}px, Height: {1}px", image.Width, image.Height);
                    Hr();
                }
                else
                {
                    DisplayLine("Kein Bild mit der Id = "+id+" gefunden!", Priority.Warn);
                }
            }
            else
            {
                DisplayLine("Ungültige Eingabe!",Priority.Warn);
            }
        }

        #endregion

        #region formatingOptions

        /// <summary>
        ///     displays a nice notification in different colors which depends on the status
        /// </summary>
        /// <param name="message">the message to display</param>
        /// <param name="status">the status / priority of the notification</param>
        /// <param name="newLine">should the method write in a new line?</param>
        private static void DisplayLine(string message, Priority status, bool newLine = true)
        {
            switch (status)
            {
                case Priority.Fatal:
                    DisplayLine("[fatal error] ", ConsoleColor.DarkRed, false);
                    break;
                case Priority.Error:
                    DisplayLine("[error] ", ConsoleColor.Red, false);
                    break;
                case Priority.Warn:
                    DisplayLine("[warning] ", ConsoleColor.Red, false);
                    break;
                case Priority.Info:
                    DisplayLine("[info] ", ConsoleColor.Yellow, false);
                    break;
                case Priority.Success:
                    DisplayLine("[success] ", ConsoleColor.Green, false);
                    break;
            }
            if (newLine)
            {
                Console.WriteLine(message);
            }
            else
            {
                Console.Write(message);
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        ///     Displays a message in a certainly color
        /// </summary>
        /// <param name="message">the message to print</param>
        /// <param name="color">the color of the message</param>
        /// <param name="newLine">should the method write in a new line?</param>
        private static void DisplayLine(string message, ConsoleColor color, bool newLine = true, bool showDate = true)
        {
            if (showDate)
            {
                Console.Write("[" + DateTime.Now.ToString("hh:mm:ss") + "]");
            }
            Console.ForegroundColor = color;
            if (newLine)
            {
                Console.WriteLine(message);
            }
            else
            {
                Console.Write(message);
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        ///     Creats a line with the exact width of the current window
        /// </summary>
        private static void Hr()
        {
            Console.BackgroundColor = ConsoleColor.Gray;
            for (int i = 0; i < Console.WindowWidth; i++)
            {
                Console.Write(" ");
            }
            Console.BackgroundColor = ConsoleColor.Black;
        }

        #endregion

        #region imageFunctions

        /// <summary>
        ///     Converts a ByteArray to an Picture
        /// </summary>
        /// <param name="byteArrayIn"></param>
        /// <returns></returns>
        private static Image ByteArrayToImage(byte[] byteArrayIn)
        {
            var ms = new MemoryStream(byteArrayIn);
            Image returnImageData = Image.FromStream(ms);
            return returnImageData;
        }

        /// <summary>
        ///     Converts a Picture to a ByteArray
        /// </summary>
        /// <param name="imageIn"></param>
        /// <returns></returns>
        private static byte[] ImageToByteArray(Image imageIn)
        {
            var ms = new MemoryStream();
            imageIn.Save(ms, ImageFormat.Jpeg);
            return ms.ToArray();
        }

        #endregion
    }
}