﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TCPServer
{
    internal class User
    {
        public User()
        {
            RegisterDate = DateTime.Now;
        }

        [Required]
        public string Name { get; set; }

        public int UserId { get; set; }

        public string Password { get; set; }

        public DateTime RegisterDate { get; set; }
    }
}