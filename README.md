# TCP Server
## Commands
**Device:**

*Write:*
* ```00<User>:<Password>``` - Anmeldung 
* ```01<Base64>``` - Bild Upload
* ```02<ID>``` - Einzelnes Bild abfragen
* ```03``` - Alle Bild IDs
* ```04<ID>:<Wert(0-100)>``` - Bearbeitet das Bild (Schwarz-Weiß)
* ```05<ID>:<Width>:<Height>``` - Gibt das Bild in der angegebenen Größe zurück
* ```06<ID>``` - Löschen
* ```07<text>``` - Kommando um gegebenen text zu zeichnen
* ```08<bildID>;<threshholdValue>``` - Zeichnet Bild mit der gegebenen ID und gegebenen Threshhold-Wert
* ```99``` - Disconnect

*Read:*
* ```00<true / false>``` - Anmeldung(Erfolgreich / Fehler)
* ```01<Bild ID>``` - Liefert ID des zuletzt geuploadeten Bild
* ```02<Base64 / false>``` - Base64 String
* ```03<ID1;ID2;ID3;...>``` - Liefert IDs aller gespeicherten Bilder

Registration Token needs to be sent directly after login was successfull.